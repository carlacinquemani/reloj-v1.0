#ifndef ALARMA_H
#define ALARMA_H

#include <stdint.h>
#include "constantes.h"

/**
 * @brief Inicializa la alarma.
 *
 * Setea los valores iniciales 00:00 para hora y todos los dias en false
 */
void inicializarAlarma();

/**
 * @brief Setea la hora de la alarma
 *
 * Configura la hora a la que debe sonar la alarma.
 * Cuando el parametro es incorrecto, no se modifica la hora actual.
 *
 * @param hora - la hora a la que debe sonar la alarma. 0 <= hora <= 23
 */
void setHoraAlarma(int hora);

/**
 * @brief Setea el minuto de la alarma.
 *
 * Configura el minuto al que debe sonar la alarma.
 * Cuando el parametro es incorrecto, no se modifica el valor actual
 *
 * @param minuto - el minuto a la que debe sonar la alarma. 0 <= minuto <= 59
 */
void setMinutoAlarma(int minuto);

/**
 * @brief Modifica el valor del dia ON/OFF (true/false)
 *
 * Cambia el valor del dia de ON a OFF y viceversa.
 * Cuando el parametro es incorrecto, no se realiza ninguna modificacion.
 *
 * @param dia - el dia a modificar
 */
void switchDiaAlarma(int dia);

/**
 * @brief Devuelve la hora y minuto de la alarma
 *
 * Obtiene los valores actuales para hora y minuto de la alarma.
 *
 * @param horaAlarma - el array con la hora y minuto de la alarma
 */
uint8_t * obtenerHoraAlarma();

/**
 * @brief Devuelve los dias que suena la alarma
 *
 * Obtiene el array de dias que suena la alarma.
 *
 * @param diasAlarma - el array de dias que suena la alarma.
 */
int * obtenerDiasAlarma();

#endif // ALARMA_H

