/*********************/
/*   HORA Y MINUTO   */
/*********************/

#define MAX_HORA 23
#define MAX_MINUTO 59



/*************************/
/*   DIAS DE LA SEMANA   */
/*************************/

#define DOMINGO     0
#define LUNES       1
#define MARTES      2
#define MIERCOLES   3
#define JUEVES      4
#define VIERNES     5
#define SABADO      6
