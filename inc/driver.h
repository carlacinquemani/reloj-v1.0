#ifndef DRIVER_H
#define DRIVER_H

/*==========================[INCLUDES]===============================*/
#include <stdint.h>


/*==========================[DEFINES]===============================*/

#define lpc4337            1
#define mk60fx512vlq15     2

#define ENTRADA 0
#define SALIDA  1

// Defino un solo puerto para todos los transistores ya que al cabo es el mismo puerto para todos
#define DISPLAY_GPIO_PORT    3

// Transistor 1 - DECENA DE HORA
#define DECHORA_MUX_GROUP    6
#define DECHORA_MUX_PIN      4
#define DECHORA_GPIO_PORT    3
#define DECHORA_GPIO_PIN     3


// Transistor 2 - UNIDAD DE HORA
#define UNIHORA_MUX_GROUP    6
#define UNIHORA_MUX_PIN      5
#define UNIHORA_GPIO_PORT    3
#define UNIHORA_GPIO_PIN     4

// Transistor 3 - DECENA DE MINUTO
#define DECMIN_MUX_GROUP    6
#define DECMIN_MUX_PIN      9
#define DECMIN_GPIO_PORT    3
#define DECMIN_GPIO_PIN     5

// Transistor 4 - UNIDAD DE MINUTO
#define UNIMIN_MUX_GROUP    6
#define UNIMIN_MUX_PIN      10
#define UNIMIN_GPIO_PORT    3
#define UNIMIN_GPIO_PIN     6

//Se define asi porque el puerto en los digitos es el mismo (GPIO2)

#define LED_GPIO_PORT    2

// DIGITO A
#define LEDA_MUX_GROUP    4
#define LEDA_MUX_PIN      0
#define LEDA_GPIO_PORT    2
#define LEDA_GPIO_PIN     0

// DIGITO B
#define LEDB_MUX_GROUP    4
#define LEDB_MUX_PIN      1
#define LEDB_GPIO_PORT    2
#define LEDB_GPIO_PIN     1

// DIGITO C
#define LEDC_MUX_GROUP    4
#define LEDC_MUX_PIN      2
#define LEDC_GPIO_PORT    2
#define LEDC_GPIO_PIN     2

// DIGITO D
#define LEDD_MUX_GROUP    4
#define LEDD_MUX_PIN      3
#define LEDD_GPIO_PORT    2
#define LEDD_GPIO_PIN     3

// DIGITO E
#define LEDE_MUX_GROUP    4
#define LEDE_MUX_PIN      4
#define LEDE_GPIO_PORT    2
#define LEDE_GPIO_PIN     4

// DIGITO F
#define LEDF_MUX_GROUP    4
#define LEDF_MUX_PIN      5
#define LEDF_GPIO_PORT    2
#define LEDF_GPIO_PIN     5

// DIGITO G
#define LEDG_MUX_GROUP    4
#define LEDG_MUX_PIN      6
#define LEDG_GPIO_PORT    2
#define LEDG_GPIO_PIN     6

// DIGITO ':'
#define LEDP_MUX_GROUP    6
#define LEDP_MUX_PIN      1
#define LEDP_GPIO_PORT    3
#define LEDP_GPIO_PIN     0

// LED DOMINGO
#define LEDDOM_MUX_GROUP    6
#define LEDDOM_MUX_PIN      8
#define LEDDOM_GPIO_PORT    5
#define LEDDOM_GPIO_PIN     16

// LED LUNES
#define LEDLUN_MUX_GROUP    6
#define LEDLUN_MUX_PIN      7
#define LEDLUN_GPIO_PORT    5
#define LEDLUN_GPIO_PIN     15

// LED MARTES
#define LEDMAR_MUX_GROUP    4
#define LEDMAR_MUX_PIN      10
#define LEDMAR_GPIO_PORT    5
#define LEDMAR_GPIO_PIN     14

// LED MIERCOLES
#define LEDMIE_MUX_GROUP    4
#define LEDMIE_MUX_PIN      8
#define LEDMIE_GPIO_PORT    5
#define LEDMIE_GPIO_PIN     12

// LED JUEVES
#define LEDJUE_MUX_GROUP    3
#define LEDJUE_MUX_PIN      2
#define LEDJUE_GPIO_PORT    5
#define LEDJUE_GPIO_PIN     9

// LED VIERNES
#define LEDVIE_MUX_GROUP    4
#define LEDVIE_MUX_PIN      9
#define LEDVIE_GPIO_PORT    5
#define LEDVIE_GPIO_PIN     13

// LED SABADO
#define LEDSAB_MUX_GROUP    3
#define LEDSAB_MUX_PIN      1
#define LEDSAB_GPIO_PORT    5
#define LEDSAB_GPIO_PIN     8

// SWITCH MODO

// MODO ALARMA
#define MODOALRM_MUX_GROUP    7
#define MODOALRM_MUX_PIN      5
#define MODOALRM_GPIO_PORT    3
#define MODOALRM_GPIO_PIN     13

// SETHORA
#define SETHORA_MUX_GROUP    7
#define SETHORA_MUX_PIN      7
#define SETHORA_GPIO_PORT    3
#define SETHORA_GPIO_PIN     15

//PUERTO DE BOCINA
#define BUZZER_MUX_GROUP    1
#define BUZZER_MUX_PIN      5
#define BUZZER_GPIO_PORT    1
#define BUZZER_GPIO_PIN     8


//#define KEY2_GPIO_PORT    2

/*
 *F1 {'1','2','3'},
  F2 {'4','5','6'},
  F3 {'7','8','9'},
  F4 {'*','0','#'}
 *    C1 ,C2 , C3
 */


// FILA 1 (1,2,3)
#define FILA1_MUX_GROUP    1
#define FILA1_MUX_PIN      17
#define FILA1_GPIO_PORT    0
#define FILA1_GPIO_PIN     12

// FILA 2 (4,5,6)
#define FILA2_MUX_GROUP    1
#define FILA2_MUX_PIN      4
#define FILA2_GPIO_PORT    0
#define FILA2_GPIO_PIN     11

// FILA 3 (7,8,9)
#define FILA3_MUX_GROUP    1
#define FILA3_MUX_PIN      3
#define FILA3_GPIO_PORT    0
#define FILA3_GPIO_PIN     10

// FILA 4 (*,0,#)
#define FILA4_MUX_GROUP    1
#define FILA4_MUX_PIN      16
#define FILA4_GPIO_PORT    0
#define FILA4_GPIO_PIN     3

// COLUMNA 1 (1,4,7,*)
#define COL1_MUX_GROUP    1
#define COL1_MUX_PIN      15
#define COL1_GPIO_PORT    0
#define COL1_GPIO_PIN     2

// COLUMNA 2 (2,5,8,0)
#define COL2_MUX_GROUP    0
#define COL2_MUX_PIN      1
#define COL2_GPIO_PORT    0
#define COL2_GPIO_PIN     1

// COLUMNA 3 (3,6,9,#)
#define COL3_MUX_GROUP    0
#define COL3_MUX_PIN      0
#define COL3_GPIO_PORT    0
#define COL3_GPIO_PIN     0


// Valores a devolver segun la posicion del switch
#define SWITCH_NORMAL       0
#define SWITCH_SET_HORA     1
#define SWITCH_SET_ALARMA   2


/*==========================[FUNCIONES]===============================*/
/**
 * @brief Inicializa el HW del micro
 *
 * Setea los puertos de E/S y a que dispositivo estan conectados
 */
void initHW(void);

/**
 * @brief Delay en milisegundos
 *
 * @param ms milisegundos de delay
 */
void delay(int ms);

// LECTURA DE ENTRADAS

/**
 * @brief Lee la posicion actual del switch
 *Lee puerto a puerto los valores de entrada de SET_HORA y SET_ALRM
 * Valores posibles:
 *  - NORMAL: funcionamiento normal del reloj
 *  - SET_HORA: modo para configurar la hora y dia actual
 *  - SET_ALARMA: modo para configurar la hora y dia que suena la alarma
 *
 * @return la posicion del switch como un entero (SWITCH_NORMAL, SWITCH_SET_ALARMA, SWITCH_SET_HORA)
 */
int leerPosicionSwitch();

/**
 * @brief Lee el valor ingresado por teclado
 *
 * Analiza los puertos del teclado y determina si hay una tecla presionada.
 * El valor leido se almacena en el parametro de entrada
 *
 * @param valorLeido Puntero a la variable donde se almacena el valor leido
 * @return TRUE si se leyo un valor, FALSE en otro caso
 */
uint8_t leerTeclado();

// MANEJO DE SALIDAS

/**
 * @brief Enciende el led correspondiente al dia
 *
 * Enciende el led correspondiente al dia especificado y apaga los otros
 *
 * @param dia el dia a modificar (DOM - SAB)
 */
void prenderDia(uint8_t dia);

/**
 * @brief Modifica el dia indicado dentro del vector de dias de la alarma
 *
 * Cambia TRUE <-> FALSE el dia indicado dentro del vector de dias de alarma
 *
 * @param dia el dia a modificar (DOMINGO - SABADO)
 */
void switchDia(uint8_t dia);

/**
 * @brief Setea un valor en el display 7 segmentos indicado
 *
 * @param display vector con numeros a encender
 */
void display(uint8_t display[]);

/**
 * @brief Si tiene un valor '1' se enciende el Buzzer
 * Si tiene valor '0' permanece apagado
 */
void BuzzerOn(void);

// MANEJO DE BANDERAS

/**
 * @brief Titila el campo hora del display
 *
 * Indica que se deben poner a titilar los display 7 segmentos correspondientes
 * a la hora del reloj.
 *
 * Se debe llamar a esta funcion cuando se va a configurar este campo, ya sea
 * para configurar la hora o la alarma
 */
void titilarHora(void);

/**
 * @brief Titila el campo minuto del display
 *
 * Indica que se deben poner a titilar los display 7 segmentos correspondientes
 * al minuto del reloj.
 *
 * Se debe llamar a esta funcion cuando se va a configurar este campo, ya sea
 * para configurar la hora o la alarma
 */
void titilarMinuto(void);

/**
 * @brief Titila el/los led/s de dia
 *
 * Indica que se deben poner a titilar los LEDS correspondientes al dia.
 *
 * Se debe llamar a esta funcion cuando se va a configurar este campo, ya sea
 * para configurar la hora (un dia solo) o la alarma (varios dias)
 */
void titilarDias(void);

/**
 * @brief Detiene el titilar en todos los leds
 *
 * Indica que se debe dejar de titilar en todos los campos. Se debe llamar antes de
 * salir de una funcion de configuracion.
 */
void stopTitilar(void);

void stopTitilarDias(void);

/**
 * @brief Muestra el vector de dias de alarma
 *
 * Indica que se debe mostrar el vector de dias que tiene configurada la alarma en vez
 * de un dia solo.
 *
 * Se debe llamar al momento de iniciar la rutina de configuracion de alarma.
 */
void mostrarDiasAlarma(void);

/**
 * @brief Muestra solamente un dia en el set de leds de dias de la semana
 *
 * Indica que se debe mostrar un dia solo en vez del vector de dias.
 *
 * Se debe llamar al momento de iniciar la rutina normal o la de configuracion de hora.
 */
void mostrarDiaSolo(void);

/**
 * @brief Indica que se esta en la rutina normal
 *
 * Funcion para indicar al driver si actualmente se esta en la rutina normal o no
 *
 * @param nuevoValor true si se pasa a rutina normal, false en otro caso
 */
void switchRutinaNormal(int nuevoValor);

/*==========================[Fin: FUNCIONES]===============================*/


#if (CPU == mk60fx512vlq15)
/* Reset_Handler is defined in startup_MK60F15.S_CPP */
void Reset_Handler( void );

extern uint32_t __StackTop;
#elif (CPU == lpc4337)



extern void ResetISR(void);

/** \brief Stack Top address
 **
 ** External declaration for the pointer to the stack top from the Linker Script
 **
 ** \remark only a declaration is needed, there is no definition, the address
 **         is set in the linker script:
 **         externals/base/cortexM4/lpc43xx/linker/ciaa_lpc4337.ld.
 **/
extern void _vStackTop(void);
#else
#endif


#endif // DRIVER_H

