#ifndef DIA_H
#define DIA_H

#include <stdint.h>
#include "constantes.h"

/**
 * @brief Inicializa Dia con los valores por defecto
 *
 * Setea el dia en DOMINGO inicialmente
 */
void inicializarDia(void);

/**
 * @brief Devuelve el dia actual
 *
 * Obtiene el valor actual del dia (DOM - SAB)
 *
 * @return el dia actual en su valor numerico definido en constantes.h
 */
uint8_t obtenerDiaActual();

/**
 * @brief Cambia el valor del dia actual
 *
 * Cambia el valor del dia actual por el parametro. Si el valor no esta
 * dentro de los esperados (DOM = 0, SAB = 6), se fija en el maximo o
 * minimo segun corresponda
 *
 * @param dia el dia a establecer
 */
void setearDia(int dia);

/**
 * @brief Incrementa el dia actual
 *
 * Incrementa el dia actual en forma ciclica (SAB -> DOM)
 */
void incrementarDia();

#endif // DIA_H

