#ifndef HORA_H
#define HORA_H

#include <stdint.h>
#include "constantes.h"

/**
 * @brief Setea la hora actual a valores por defecto
 *
 * Inicializa la hora en 00:00 (24hs)
 */
void inicializarHora(void);

/**
 * @brief Obtiene la hora actual
 *
 * Devuelve la hora actual como un puntero
 *
 * @return puntero al primer elemento del array
 */
uint8_t * obtenerHora();

/**
 * @brief Obtiene el vector de hora set actual
 *
 * Devuelve la hora set como un puntero
 *
 * @return puntero al primer elemento del array
 */
uint8_t * obtenerHoraSet();

/**
 * @brief Incrementa el valor de la hora
 *
 * Incrementa la hora en un minuto y modifica el resto de la
 * hora actual consecuentemente
 */
void incrementarHora(void);

/**
 * @brief Setea la hora actual
 *
 * Establece el valor de horas y minutos segun los parametros. Si alguno de
 * los mismos tiene un valor no permitido, no se realiza ningun cambio
 *
 * @param horas debe ser un valor entre 0 y 23
 * @param minutos debe ser un valor entre 0 y 59
 */
void setearHora(uint8_t horas, uint8_t minutos);

/**
 * @brief Agrega un digito a los digitos correspondientes a las horas
 *
 * Ingresa un digito de tal manera que siempre se mantenga un nro entre 0 y 23
 * Tabla logica:
 * +================================+
 * | Actual | Secuencia | Resultado |
 * |========+===========+===========|
 * |   XY   |     0     |     0Y    |
 * |   XY   |     1     |     1Y    |
 * | X(Y<=3)|     2     |     2Y    |
 * | X(Y>3) |     2     |     20    |
 * |   XY   |    A>=3   |     0A    |
 * |   --   |    0, A   |     0A    |
 * |   --   |    1, A   |     1A    |
 * |   --   |  2, A<=3  |     2A    |
 * |   --   |   2, A>3  |     0A    |
 * |   --   |    3,0    |     03    |
 * |   --   |    3,1    |     13    |
 * |   --   |    3,2    |     23    |
 * |   --   |  3, A>=3  |     0A    |
 * |   --   |   A>3, 0  |     0A    |
 * |   --   |   A>3, 1  |     1A    |
 * |   --   |   A>3, 2  |     20    |
 * |   --   |  A>3, B>2 |     0B    |
 * +================================+
 * Si el parametro es mayor que 9, no se hace nada
 *
 * @param digito el digito a agregar (debe ser entre 0 y 9)
 */
void agregarDigitoHora(uint8_t digito);

/**
 * @brief Agrega un digito a los correspondientes a los minutos
 *
 * Ingresa un digito de tal manera que siempre se mantenga un nro entre 0 y 59
 * Tabla logica:
 * +================================+
 * | Actual | Secuencia | Resultado |
 * |========+===========+===========|
 * |   XY   |    A<6    |     AY    |
 * |   XY   |   A>=6    |     0A    |
 * |   --   |   A<6, B  |     AB    |
 * |   --   | A>=6, B<6 |     BA    |
 * |   --   | A>=6, B>=6|     0B    |
 * +================================+
 * Si el parametro es mayor que 9, no se hace nada
 *
 * @param digito el digito a agregar (debe ser entre 0 y 9)
 */
void agregarDigitoMinuto(uint8_t digito);

/**
 * @brief Determina si la hora actual es 00:00
 * @return TRUE si hora actual es la hora inicial, FALSE en otro caso
 */
int esHoraInicial();

/**
 * @brief Setea un nuevo valor para modificarDecena
 * @param modificarDecena
 */
void modificarDecena(int nuevoValor);

/**
 * @brief Copia el vector de hora actual en el vector especificado
 * @param hora el vector donde se copia la hora actual
 */
void copiarHoraActualParaSet();

/**
 * @brief Establece la hora actual de acuerdo a lo seteado
 */
void establecerHora();

#endif // HORA_H

