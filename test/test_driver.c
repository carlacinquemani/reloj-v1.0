#include "unity.h"

void test_valoresLimites_incrementarMaxInt_vuelveMaxNegativo() {
    int counter = 2147483647;
    counter++;
    TEST_ASSERT_EQUAL_INT(-2147483648, counter);
}

void test_valoresLimites_incrementarCercaDelLimite_vuelveMaxNegativoMas() {
    int counter = 2147483640;
    int counterDia = counter + 10;
    TEST_ASSERT_EQUAL_INT(-2147483646, counterDia);
}

void test_valoresLimites_uint8t_vuelveCero() {
    uint8_t counter = 255;
    counter++;
    TEST_ASSERT_EQUAL_UINT8(0, counter);
}

void test_valoresLimites_uint16t_vuelveCero() {
    uint16_t counter = 65535;
    counter++;
    TEST_ASSERT_EQUAL_UINT8(0, counter);
}

void test_valores_corrimiento_bits() {
    const uint8_t arrayCorrimiento[4] = {8 << 0, 8 << 1, 8 << 2, 8 << 3};

    for(int i = 0; i < sizeof(arrayCorrimiento); i++) printf("%d -", arrayCorrimiento[i]);
}
