#include "unity.h"
#include "alarma.h"

void test_inicializarAlarma_setea_valores_correcto(void) {
    const uint8_t RESULTADO_HORA[4] = {0};
    const int RESULTADO_DIAS[7] = {0};
    uint8_t * hora;
    int * dias;

    inicializarAlarma();
    hora = obtenerHoraAlarma();
    dias = obtenerDiasAlarma();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO_HORA, hora, 4);
    TEST_ASSERT_EQUAL_INT_ARRAY(RESULTADO_DIAS, dias, 7);
}

void test_setHoraAlarma_setea_hora_correcta(void) {
    const uint8_t RESULTADO[4] = {1, 5, 0, 0};
    uint8_t * hora;

    inicializarAlarma();
    setHoraAlarma(15);
    hora = obtenerHoraAlarma();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_setHoraAlarma_no_modifica_con_parametro_mayor_al_permitido(void) {
    const uint8_t RESULTADO[4] = {1, 5, 0, 0};
    uint8_t * hora;

    inicializarAlarma();
    setHoraAlarma(15);
    setHoraAlarma(24);
    hora = obtenerHoraAlarma();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_setHoraAlarma_no_modifica_con_parametro_negativo(void) {
    const uint8_t RESULTADO[4] = {1, 5, 0, 0};
    uint8_t * hora;

    inicializarAlarma();
    setHoraAlarma(15);
    setHoraAlarma(-1);
    hora = obtenerHoraAlarma();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_setMinutoAlarma_setea_minuto_correcto(void) {
    const uint8_t RESULTADO[4] = {0, 0, 3, 2};
    uint8_t * hora;

    inicializarAlarma();
    setMinutoAlarma(32);
    hora = obtenerHoraAlarma();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_setMinutoAlarma_no_modifica_con_parametro_mayor_al_permitido(void) {
    const uint8_t RESULTADO[4] = {0, 0, 3, 2};
    uint8_t * hora;

    inicializarAlarma();
    setMinutoAlarma(32);
    setMinutoAlarma(60);
    hora = obtenerHoraAlarma();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_setMinutoAlarma_no_modifica_con_parametro_negativo(void) {
    const uint8_t RESULTADO[4] = {0, 0, 3, 2};
    uint8_t * hora;

    inicializarAlarma();
    setMinutoAlarma(32);
    setMinutoAlarma(-10);
    hora = obtenerHoraAlarma();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_setHoraAlarma_setMinutoAlarma_funcionan_correcto_juntos(void) {
    const uint8_t RESULTADO[4] = {2, 0, 1, 5};
    uint8_t * hora;

    inicializarAlarma();
    setHoraAlarma(20);
    setMinutoAlarma(15);
    hora = obtenerHoraAlarma();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_switchDiaAlarma_cambia_valor_dia_semana_solo(void) {
    const int RESULTADO[7] = {0, 1, 0, 0, 0, 0, 0};
    int * diasAlarma;

    inicializarAlarma();
    switchDiaAlarma(LUNES);
    diasAlarma = obtenerDiasAlarma();

    TEST_ASSERT_EQUAL_INT_ARRAY(RESULTADO, diasAlarma, 7);
}

void test_switchDiaAlarma_cambia_valor_lunes_a_viernes(void) {
    const int RESULTADO[7] = {0, 1, 1, 1, 1, 1, 0};
    int * diasAlarma;

    inicializarAlarma();
    switchDiaAlarma(LUNES);
    switchDiaAlarma(MARTES);
    switchDiaAlarma(MIERCOLES);
    switchDiaAlarma(JUEVES);
    switchDiaAlarma(VIERNES);
    diasAlarma = obtenerDiasAlarma();

    TEST_ASSERT_EQUAL_INT_ARRAY(RESULTADO, diasAlarma, 7);
}

void test_switchDiaAlarma_no_modifica_con_valor_invalido(void) {
    const int RESULTADO[7] = {0, 1, 1, 0, 0, 0, 0};
    int * diasAlarma;

    inicializarAlarma();
    switchDiaAlarma(LUNES);
    switchDiaAlarma(MARTES);
    switchDiaAlarma(7);
    switchDiaAlarma(-1);
    diasAlarma = obtenerDiasAlarma();

    TEST_ASSERT_EQUAL_INT_ARRAY(RESULTADO, diasAlarma, 7);
}

void test_switchDiaAlarma_vuelve_valor_inicial_con_dos_switches(void) {
    const int RESULTADO[7] = {0, 0, 1, 1, 0, 0, 0};
    int * diasAlarma;

    inicializarAlarma();
    switchDiaAlarma(LUNES);
    switchDiaAlarma(MARTES);
    switchDiaAlarma(MIERCOLES);
    switchDiaAlarma(LUNES);
    diasAlarma = obtenerDiasAlarma();

    TEST_ASSERT_EQUAL_INT_ARRAY(RESULTADO, diasAlarma, 7);
}
