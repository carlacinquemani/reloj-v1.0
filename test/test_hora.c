#include "unity.h"
#include "hora.h"

// DECLARACION FUNCIONES AUXILIARES
void agregarDigitoHora_initSetAndAssert_1(const uint8_t expected[], uint8_t horaSet, uint8_t minutoSet, uint8_t digito);
void agregarDigitoHora_initSetAndAssert_2(const uint8_t expected[], uint8_t horaSet, uint8_t minutoSet, uint8_t d1, uint8_t d2);
void agregarDigitoMinuto_initSetAndAssert_1(const uint8_t expected[], uint8_t horaSet, uint8_t minutoSet, uint8_t digito);
void agregarDigitoMinuto_initSetAndAssert_2(const uint8_t expected[], uint8_t horaSet, uint8_t minutoSet, uint8_t d1, uint8_t d2);

// TESTS
void test_inicializarHora_setea_horaActual_en_cero(void) {
    const uint8_t RESULTADO[4] = {0};
    uint8_t * hora;

    inicializarHora();
    hora = obtenerHora();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_incrementarHora_incrementa_minuto_en_uno(void) {
    const uint8_t RESULTADO[4] = {0, 0, 0, 1};
    uint8_t * hora;

    inicializarHora();
    incrementarHora();
    hora = obtenerHora();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_incrementarHora_incrementa_dos_minutos(void) {
    const uint8_t RESULTADO[4] = {0, 0, 0, 2};
    uint8_t * hora;

    inicializarHora();
    incrementarHora();
    incrementarHora();
    hora = obtenerHora();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_incrementarHora_incrementa_decena_si_incrementa_10_veces(void) {
    const uint8_t RESULTADO[4] = {0, 0, 1, 0};
    uint8_t * hora;

    inicializarHora();

    for(int i = 0; i < 10; i++) {
        incrementarHora();
    }

    hora = obtenerHora();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_incrementarHora_incrementa_unidad_hora_si_incrementa_60_veces(void) {
    const uint8_t RESULTADO[4] = {0, 1, 0, 0};
    uint8_t * hora;

    inicializarHora();

    for(int i = 0; i < 60; i++) {
        incrementarHora();
    }

    hora = obtenerHora();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_incrementarHora_incrementa_decena_hora(void) {
    const uint8_t RESULTADO[4] = {1, 0, 0, 0};
    uint8_t * hora;

    inicializarHora();

    for(int i = 0; i < 600; i++) {
        incrementarHora();
    }

    hora = obtenerHora();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_incrementarHora_reinicia_si_incrementa_24_horas(void) {
    const uint8_t RESULTADO[4] = {0, 0, 0, 0};
    uint8_t * hora;

    inicializarHora();

    for(int i = 0; i < 60 * 24; i++) {
        incrementarHora();
    }

    hora = obtenerHora();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_setarHora_setea_hora_establecida(void) {
    const uint8_t RESULTADO[4] = {1, 5, 3, 9};
    uint8_t * hora;

    inicializarHora();
    setearHora(15, 39);

    hora = obtenerHora();
    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_setarHora_no_hace_nada_si_horas_incorrecta() {
    const uint8_t RESULTADO[4] = {0, 0, 0, 0};
    uint8_t * hora;

    inicializarHora();
    setearHora(30, 39);

    hora = obtenerHora();
    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_setarHora_no_hace_nada_si_minutos_incorrecta() {
    const uint8_t RESULTADO[4] = {0, 0, 0, 0};
    uint8_t * hora;

    inicializarHora();
    setearHora(10, 80);

    hora = obtenerHora();
    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO, hora, 4);
}

void test_esHoraInicial_devuelveTrue_siRecibeVectorInicial() {
    inicializarHora();

    TEST_ASSERT_TRUE(esHoraInicial());
}

void test_esHoraInicial_devuelveFalse_siRecibeVectorDistintoInicial() {
    inicializarHora();
    incrementarHora();

    TEST_ASSERT_FALSE(esHoraInicial());
}

void test_agregarDigitoHora_noHaceNada_siParametroMayorQue9() {
    const uint8_t RESULTADO[4] = {2, 1, 3, 5};
    agregarDigitoHora_initSetAndAssert_1(RESULTADO, 21, 35, 10);
}

void test_agregarDigitoHora_devuelveHora0Y_siIngreso_0() {
    const uint8_t RESULTADO[4] = {0, 5, 0, 0};
    agregarDigitoHora_initSetAndAssert_1(RESULTADO, 15, 0, 0);
}

void test_agregarDigitoHora_devuelveHora1Y_siIngreso_1() {
    const uint8_t RESULTADO[4] = {1, 3, 0, 0};
    agregarDigitoHora_initSetAndAssert_1(RESULTADO, 23, 0, 1);
}

void test_agregarDigitoHora_devuelveHora23_siIngreso_2_yActual13() {
    const uint8_t RESULTADO[4] = {2, 3, 0, 0};
    agregarDigitoHora_initSetAndAssert_1(RESULTADO, 13, 0, 2);
}

void test_agregarDigitoHora_devuelveHora20_siIngreso_2_yActual04() {
    const uint8_t RESULTADO[4] = {2, 0, 0, 0};
    agregarDigitoHora_initSetAndAssert_1(RESULTADO, 4, 0, 2);
}

void test_agregarDigitoHora_devuelveHora03_siIngreso_3_yActualXY() {
    const uint8_t RESULTADO[4] = {0, 3, 0, 0};
    agregarDigitoHora_initSetAndAssert_1(RESULTADO, 17, 0, 3);
}

void test_agregarDigitoHora_devuelveHora08_siIngreso_8_yActualXY() {
    const uint8_t RESULTADO[4] = {0, 8, 0, 0};
    agregarDigitoHora_initSetAndAssert_1(RESULTADO, 22, 0, 8);
}

void test_agregarDigitoHora_devuelveHora05_siIngreso_0_5() {
    const uint8_t RESULTADO[4] = {0, 5, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 19, 0, 0, 5);
}

void test_agregarDigitoHora_devuelveHora02_siIngreso_0_2() {
    const uint8_t RESULTADO[4] = {0, 2, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 19, 0, 0, 2);
}

void test_agregarDigitoHora_devuelveHora15_siIngreso_1_5() {
    const uint8_t RESULTADO[4] = {1, 5, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 23, 0, 1, 5);
}

void test_agregarDigitoHora_devuelveHora12_siIngreso_1_2() {
    const uint8_t RESULTADO[4] = {1, 2, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 19, 0, 1, 2);
}

void test_agregarDigitoHora_devuelveHora23_siIngreso_2_3_yActual15() {
    const uint8_t RESULTADO[4] = {2, 3, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 15, 0, 2, 3);
}

void test_agregarDigitoHora_devuelveHora23_siIngreso_2_3_yActual02() {
    const uint8_t RESULTADO[4] = {2, 3, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 2, 0, 2, 3);
}

void test_agregarDigitoHora_devuelveHora04_siIngreso_2_4() {
    const uint8_t RESULTADO[4] = {0, 4, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 15, 0, 2, 4);
}

void test_agregarDigitoHora_devuelveHora08_siIngreso_2_8() {
    const uint8_t RESULTADO[4] = {0, 8, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 20, 0, 2, 8);
}

void test_agregarDigitoHora_devuelveHora03_siIngreso_3_0() {
    const uint8_t RESULTADO[4] = {0, 3, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 22, 0, 3, 0);
}

void test_agregarDigitoHora_devuelveHora13_siIngreso_3_1() {
    const uint8_t RESULTADO[4] = {1, 3, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 8, 0, 3, 1);
}

void test_agregarDigitoHora_devuelveHora23_siIngreso_3_2() {
    const uint8_t RESULTADO[4] = {2, 3, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 15, 0, 3, 2);
}

void test_agregarDigitoHora_devuelveHora03_siIngreso_3_3() {
    const uint8_t RESULTADO[4] = {0, 3, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 19, 0, 3, 3);
}

void test_agregarDigitoHora_devuelveHora07_siIngreso_3_7() {
    const uint8_t RESULTADO[4] = {0, 7, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 10, 0, 3, 7);
}

void test_agregarDigitoHora_devuelveHora04_siIngreso_4_0() {
    const uint8_t RESULTADO[4] = {0, 4, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 12, 0, 4, 0);
}

void test_agregarDigitoHora_devuelveHora14_siIngreso_4_1() {
    const uint8_t RESULTADO[4] = {1, 4, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 21, 0, 4, 1);
}

void test_agregarDigitoHora_devuelveHora20_siIngreso_4_2() {
    const uint8_t RESULTADO[4] = {2, 0, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 20, 0, 4, 2);
}

void test_agregarDigitoHora_devuelveHora03_siIngreso_4_3() {
    const uint8_t RESULTADO[4] = {0, 3, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 23, 0, 4, 3);
}

void test_agregarDigitoHora_devuelveHora07_siIngreso_4_7() {
    const uint8_t RESULTADO[4] = {0, 7, 0, 0};
    agregarDigitoHora_initSetAndAssert_2(RESULTADO, 10, 0, 4, 7);
}

void test_agregarDigitoMinuto_noHaceNada_siDigitoMayorQue9() {
    const uint8_t RESULTADO[4] = {2, 1, 3, 5};
    agregarDigitoMinuto_initSetAndAssert_1(RESULTADO, 21, 35, 10);
}

void test_agregarDigitoMinuto_devuelveMinutoAY_siDigitoMenorQue6() {
    const uint8_t RESULTADO[] = {0, 0, 5, 3};
    agregarDigitoMinuto_initSetAndAssert_1(RESULTADO, 0, 33, 5);
}

void test_agregarDigitoMinuto_devuelveMinuto0A_siDigitoMayorOIgualQue6() {
    const uint8_t RESULTADO[] = {0, 0, 0, 6};
    agregarDigitoMinuto_initSetAndAssert_1(RESULTADO, 0, 33, 6);
}

void test_agregarDigitoMinuto_devuelveMinutoAB_siSecuencia_AMenorQue6_B() {
    const uint8_t RESULTADO[] = {0, 0, 5, 9};
    agregarDigitoMinuto_initSetAndAssert_2(RESULTADO, 0, 40, 5, 9);
}

void test_agregarDigitoMinuto_devuelveMinutoBA_siSecuencia_AMayorOIgualQue6_BMenorQue6() {
    const uint8_t RESULTADO[] = {0, 0, 5, 6};
    agregarDigitoMinuto_initSetAndAssert_2(RESULTADO, 0, 38, 6, 5);
}

void test_agregarDigitoMinuto_devuelveMinuto0B_siSecuencia_AMayorOIgualQue6_BMayorOIgualQue6() {
    const uint8_t RESULTADO[] = {0, 0, 0, 6};
    agregarDigitoMinuto_initSetAndAssert_2(RESULTADO, 0, 42, 7, 6);
}

void test_establecerHora_copiaHoraSetEnHoraActual() {
    const uint8_t RESULTADO_HORA_ACTUAL[4] = {1, 3, 2, 0};
    const uint8_t RESULTADO_SET[4] = {2, 1, 4, 7};
    uint8_t * horaActual;

    inicializarHora();
    setearHora(13, 20);
    horaActual = obtenerHora();
    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO_HORA_ACTUAL, horaActual, 4);

    modificarDecena(1);
    copiarHoraActualParaSet();

    agregarDigitoHora(2);
    agregarDigitoHora(1);
    agregarDigitoMinuto(4);
    agregarDigitoMinuto(7);

    establecerHora();
    horaActual = obtenerHora();
    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO_SET, horaActual, 4);
}

void test_copiarHoraActualParaSet_copiaValorEstablecido() {
    const uint8_t RESULTADO_SET[4] = {1, 2, 3, 4};
    uint8_t * horaSet;

    inicializarHora();
    setearHora(12, 34);
    copiarHoraActualParaSet();
    horaSet = obtenerHoraSet();

    TEST_ASSERT_EQUAL_UINT8_ARRAY(RESULTADO_SET, horaSet, 4);
}

// IMPLEMENTACION FUNCIONES AUXILIARES
void agregarDigitoHora_initSetAndAssert_1(const uint8_t expected[], uint8_t horaSet, uint8_t minutoSet, uint8_t digito) {
    uint8_t * hora;

    // Situacion inicial del reloj normal
    inicializarHora();
    setearHora(horaSet, minutoSet);

    // Rutina SET_HORA
    modificarDecena(1);
    copiarHoraActualParaSet();

    // Digitos leidos del teclado
    agregarDigitoHora(digito);

    hora = obtenerHoraSet();
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected, hora, 4);
}

void agregarDigitoHora_initSetAndAssert_2(const uint8_t expected[], uint8_t horaSet, uint8_t minutoSet, uint8_t d1, uint8_t d2) {
    uint8_t * hora;

    // Situacion inicial del reloj normal
    inicializarHora();
    setearHora(horaSet, minutoSet);

    // Rutina SET_HORA
    modificarDecena(1);
    copiarHoraActualParaSet();

    // Digitos leidos del teclado
    agregarDigitoHora(d1);
    agregarDigitoHora(d2);

    hora = obtenerHoraSet();
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected, hora, 4);
}

void agregarDigitoMinuto_initSetAndAssert_1(const uint8_t expected[], uint8_t horaSet, uint8_t minutoSet, uint8_t digito) {
    uint8_t * hora;

    // Situacion inicial del reloj normal
    inicializarHora();
    setearHora(horaSet, minutoSet);

    // Rutina SET_HORA
    modificarDecena(1);
    copiarHoraActualParaSet();

    // Digitos leidos del teclado
    agregarDigitoMinuto(digito);

    hora = obtenerHoraSet();
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected, hora, 4);
}

void agregarDigitoMinuto_initSetAndAssert_2(const uint8_t expected[], uint8_t horaSet, uint8_t minutoSet, uint8_t d1, uint8_t d2) {
    uint8_t * hora;

    // Situacion inicial del reloj normal
    inicializarHora();
    setearHora(horaSet, minutoSet);

    // Rutina SET_HORA
    modificarDecena(1);
    copiarHoraActualParaSet();

    // Digitos leidos del teclado
    agregarDigitoMinuto(d1);
    agregarDigitoMinuto(d2);

    hora = obtenerHoraSet();
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected, hora, 4);
}
