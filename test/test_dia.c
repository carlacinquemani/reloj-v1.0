#include "unity.h"
#include "dia.h"


void test_inicializarDia_inicia_domingo(void) {
    inicializarDia();
    uint8_t diaObtenido = obtenerDiaActual();

    TEST_ASSERT_EQUAL_UINT8(DOMINGO, diaObtenido);
}

void test_setearDia_modifica_diaActual(void) {
    setearDia(JUEVES);
    uint8_t diaObtenido = obtenerDiaActual();

    TEST_ASSERT_EQUAL_UINT8(JUEVES, diaObtenido);
}

void test_setearDia_resulta_SAB_si_dia_mayor_7(void) {
    setearDia(9);
    uint8_t diaObtenido = obtenerDiaActual();

    TEST_ASSERT_EQUAL_UINT8(SABADO, diaObtenido);
}

void test_setearDia_resulta_DOM_si_dia_menor_1(void) {
    setearDia(-1);
    uint8_t diaObtenido = obtenerDiaActual();

    TEST_ASSERT_EQUAL_UINT8(DOMINGO, diaObtenido);
}

void test_incrementarDia_pasa_DOM_a_LUN(void) {
    inicializarDia();
    incrementarDia();

    uint8_t diaObtenido = obtenerDiaActual();

    TEST_ASSERT_EQUAL_UINT8(LUNES, diaObtenido);
}

void test_incrementarDia_pasa_SAB_a_DOM(void) {
    setearDia(SABADO);
    incrementarDia();

    uint8_t diaObtenido = obtenerDiaActual();

    TEST_ASSERT_EQUAL_UINT8(DOMINGO, diaObtenido);
}
