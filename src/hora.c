#include "hora.h"
#include <string.h>
#include <stdio.h>

#define MAX_DIGITO 9
#define MAX_DECENA_HORA 2
#define MAX_UNIDAD_HORA 3
#define INDEX_DECENA_HORA 0
#define INDEX_UNIDAD_HORA 1
#define MAX_DECENA_MINUTO 5
#define INDEX_DECENA_MINUTO 2
#define INDEX_UNIDAD_MINUTO 3

/*==================[Funciones auxiliares: declaracion]=================*/
uint8_t extraerUnidadSexagesimal(uint8_t decena, uint8_t unidad);
void trasladarSexagesimales(uint8_t hora[], int horas, int minutos);


/*=========================[Variables internas]=========================*/
static uint8_t horaActual[4];
static uint8_t horaSet[4];
static int flagModificarDecena = 1;


/*=========================[Funciones publicas]=========================*/
void inicializarHora() {
    memset(horaActual, 0, sizeof(horaActual));
    memset(horaSet, 0, sizeof(horaSet));
}

uint8_t * obtenerHora() {
    return horaActual;
}

uint8_t * obtenerHoraSet() {
    return horaSet;
}

void incrementarHora() {
    uint8_t hora = extraerUnidadSexagesimal(horaActual[0], horaActual[1]);
    uint8_t minuto = extraerUnidadSexagesimal(horaActual[2], horaActual[3]);

    minuto++;

    if(minuto > MAX_MINUTO) {
        hora++;
        minuto = 0;
    }

    if(hora > MAX_HORA) {
        inicializarHora();
    } else {
        trasladarSexagesimales(horaActual, hora, minuto);
    }
}

void setearHora(uint8_t horas, uint8_t minutos) {

    if(horas <= MAX_HORA && minutos <= MAX_MINUTO) {
        trasladarSexagesimales(horaActual, horas, minutos);
    }

}

void agregarDigitoHora(uint8_t digito) {
    if(digito <= MAX_DIGITO) {
        if(flagModificarDecena) {
            if(digito <= MAX_DECENA_HORA) {
                horaSet[INDEX_DECENA_HORA] = digito;
                flagModificarDecena = 0;
                if(digito == MAX_DECENA_HORA && horaSet[INDEX_UNIDAD_HORA] > MAX_UNIDAD_HORA)
                    horaSet[INDEX_UNIDAD_HORA] = 0;
            } else {
                horaSet[INDEX_DECENA_HORA] = 0;
                horaSet[INDEX_UNIDAD_HORA] = digito;
            }
        } else {
            if(horaSet[INDEX_DECENA_HORA] == MAX_DECENA_HORA && digito > MAX_UNIDAD_HORA) {
                horaSet[INDEX_DECENA_HORA] = 0;
            }

            horaSet[INDEX_UNIDAD_HORA] = digito;
            flagModificarDecena = 1;
        }
    }
}

void agregarDigitoMinuto(uint8_t digito) {
    if(digito <= MAX_DIGITO) {
        if(flagModificarDecena) {
            if(digito <= MAX_DECENA_MINUTO) {
                horaSet[INDEX_DECENA_MINUTO] = digito;
                flagModificarDecena = 0;
            } else {
                horaSet[INDEX_DECENA_MINUTO] = 0;
                horaSet[INDEX_UNIDAD_MINUTO] = digito;
            }
        } else {
            horaSet[INDEX_UNIDAD_MINUTO] = digito;
            flagModificarDecena = 1;
        }
    }
}

int esHoraInicial() {
    for(int i = 0; i < sizeof(horaActual); i++) {
        if(horaActual[i] != 0) return 0;
    }

    return 1;
}

void modificarDecena(int nuevoValor) {
    flagModificarDecena = nuevoValor;
}

void copiarHoraActualParaSet() {
    memcpy(horaSet, horaActual, sizeof(horaActual));
}

void establecerHora() {
    memcpy(horaActual, horaSet, sizeof(horaSet));
}

/*===============[Funciones auxiliares: implementacion]===============*/
uint8_t extraerUnidadSexagesimal(uint8_t decena, uint8_t unidad) {
    if(decena > 9 || unidad > 9) {
        // Parametros incorrectos
        return 0;
    } else {
        return decena * 10 + unidad;
    }

}

void trasladarSexagesimales(uint8_t hora[], int horas, int minutos) {
    hora[3] = minutos % 10;
    hora[2] = minutos / 10;
    hora[1] = horas % 10;
    hora[0] = horas / 10;
}
