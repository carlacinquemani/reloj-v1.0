#include "dia.h"
#include <stdio.h>

static uint8_t diaActual;

void inicializarDia(void) {
    diaActual = DOMINGO;
}

uint8_t obtenerDiaActual() {
    return diaActual;
}

void setearDia(int dia) {
    if(dia > SABADO) {
        diaActual = SABADO;
    } else if (dia < DOMINGO) {
        diaActual = DOMINGO;
     } else {
        diaActual = dia;
    }
}

void incrementarDia() {
    diaActual++;

    if(diaActual > SABADO) diaActual = DOMINGO;
}
