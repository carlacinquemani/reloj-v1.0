#include "driver.h"
#include "hora.h"
#include "dia.h"
#include "constantes.h"
#include <stdio.h>
#include <string.h>

#ifndef CPU
#error CPU shall be defined
#endif
#if (lpc4337 == CPU)
#include "chip.h"
#elif (mk60fx512vlq15 == CPU)
#else
#endif

/*============================[Variables internas]==============================*/
// TODO Documentar uso de estas variables

uint32_t counter = 0;
const uint8_t digitoDisplay[4] = {8 << 0, 8 << 1, 8 << 2, 8 << 3};

const int diaToLedGpioPort[7] = {
    LEDDOM_GPIO_PORT,
    LEDLUN_GPIO_PORT,
    LEDMAR_GPIO_PORT,
    LEDMIE_GPIO_PORT,
    LEDJUE_GPIO_PORT,
    LEDVIE_GPIO_PORT,
    LEDSAB_GPIO_PORT
};
const int diaToLedGpioPin[7] = {
    LEDDOM_GPIO_PIN,
    LEDLUN_GPIO_PIN,
    LEDMAR_GPIO_PIN,
    LEDMIE_GPIO_PIN,
    LEDJUE_GPIO_PIN,
    LEDVIE_GPIO_PIN,
    LEDSAB_GPIO_PIN
};
const uint8_t portColumnas[3] = {COL1_GPIO_PORT, COL2_GPIO_PORT, COL3_GPIO_PORT};
const uint8_t pinColumnas[3] = {COL1_GPIO_PIN, COL2_GPIO_PIN, COL3_GPIO_PIN};

const uint8_t portFilas[4] = {FILA1_GPIO_PORT, FILA2_GPIO_PORT, FILA3_GPIO_PORT, FILA4_GPIO_PORT};
const uint8_t pinFilas[4] = {FILA1_GPIO_PIN, FILA2_GPIO_PIN, FILA3_GPIO_PIN, FILA4_GPIO_PIN};

const uint8_t mapaTeclado[4][3] = {
    {1, 2, 3},
    {4, 5, 6},
    {7, 8, 9},
    {10, 0, 11}
};

const uint8_t todos = 1 << LEDA_GPIO_PIN | 1 << LEDB_GPIO_PIN | 1 << LEDC_GPIO_PIN | 1 << LEDD_GPIO_PIN |1 << LEDE_GPIO_PIN | 1 << LEDF_GPIO_PIN | 1 << LEDG_GPIO_PIN;
const uint8_t segmentos[11] = {
   1 << LEDA_GPIO_PIN | 1 << LEDB_GPIO_PIN | 1 << LEDC_GPIO_PIN | 1 << LEDD_GPIO_PIN |1 << LEDE_GPIO_PIN | 1 << LEDF_GPIO_PIN,
   1 << LEDB_GPIO_PIN | 1 << LEDC_GPIO_PIN,
   1 << LEDA_GPIO_PIN | 1 << LEDB_GPIO_PIN | 1 <<  LEDD_GPIO_PIN |1 << LEDE_GPIO_PIN | 1 << LEDG_GPIO_PIN,
   1 << LEDA_GPIO_PIN | 1 << LEDB_GPIO_PIN | 1 << LEDC_GPIO_PIN | 1 << LEDD_GPIO_PIN | 1 << LEDG_GPIO_PIN,
   1 << LEDB_GPIO_PIN | 1 << LEDC_GPIO_PIN | 1 << LEDF_GPIO_PIN | 1 << LEDG_GPIO_PIN,
   1 << LEDA_GPIO_PIN | 1 << LEDC_GPIO_PIN | 1 << LEDD_GPIO_PIN | 1 << LEDF_GPIO_PIN | 1 << LEDG_GPIO_PIN,
   1 << LEDA_GPIO_PIN | 1 << LEDC_GPIO_PIN | 1 << LEDD_GPIO_PIN |1 << LEDE_GPIO_PIN | 1 << LEDF_GPIO_PIN | 1 << LEDG_GPIO_PIN,
   1 << LEDA_GPIO_PIN | 1 << LEDB_GPIO_PIN | 1 << LEDC_GPIO_PIN,
   1 << LEDA_GPIO_PIN | 1 << LEDB_GPIO_PIN | 1 << LEDC_GPIO_PIN | 1 << LEDD_GPIO_PIN | 1 << LEDE_GPIO_PIN | 1 << LEDF_GPIO_PIN | 1 << LEDG_GPIO_PIN,
   1 << LEDA_GPIO_PIN | 1 << LEDB_GPIO_PIN | 1 << LEDC_GPIO_PIN | 1 << LEDD_GPIO_PIN | 1 << LEDF_GPIO_PIN | 1 << LEDG_GPIO_PIN,
   1 << LEDA_GPIO_PIN | 1 << LEDD_GPIO_PIN};

uint8_t displayHora[4];
uint8_t displayDia = 0;
int displayVectorDia[7] = {0};
uint8_t tecla;

int flagTitilarHora = 0;
int flagTitilarMinuto = 0;
int flagTitilarDias = 0;
int flagMosrarDiaSolo = 1;
int esRutinaNormal = 1;

uint32_t counterMinuto = 0;


/**
 * @brief Convierte un valor 'uint8_t' a BCD
 *
 * Recibe un valor entero del vector hora.
 * Setea los segmentos del display según el parametro proporcionado.
 *
 * @param hora - Valor entero de un vector hora
 */
void intToBcd(uint8_t hora);

/**
 * @brief Limpia los LEDS dias
 */
void clearDias(void);


/**
 * @brief Indica si paso un minuto
 *
 * Calcula si pasaron 60 seg y actualiza los contadores si se cumple la condicion
 *
 * @return TRUE si se cuenta un minuto, FALSE en otro caso
 */
int pasoUnMinuto(void);

/*==================[macros and definitions]=================================*/

#define COUNT_DELAY 3000000
#define PERIODO_DISPLAY 20 // Periodo del display en ms
#define PERIODO_TITILAR 1000
// Se deja este valor para pruebas, se debe poner el valor comentado
#define MSEG_POR_MINUTO 1000 // 1000 * 60


void configurarInterrupcion(void) {
  /* Deshabilita interrupciones */
   asm volatile ("cpsid i");

   /* Activate SysTick */
   SystemCoreClockUpdate();
   SysTick_Config(SystemCoreClock/1000);

   /* Update priority set by SysTick_Config */
   NVIC_SetPriority(SysTick_IRQn, (1  <<__NVIC_PRIO_BITS) - 1);

  /* Habilita interrupciones */
   asm volatile ("cpsie i");
}
/*==================[external functions definition]==========================*/


/**
 * @brief Funcion periodica que se ejecuta con el SysTick
 */
void funcionPeriodica(void) {
    // Seteo de periodos de refresco
    int periodoHora = flagTitilarHora ? PERIODO_TITILAR : PERIODO_DISPLAY;
    int periodoMinuto = flagTitilarMinuto ? PERIODO_TITILAR : PERIODO_DISPLAY;
    int periodoDias = flagTitilarDias ? PERIODO_TITILAR : PERIODO_DISPLAY;

    // Actualizacion de hora, minuto y dia
    if(pasoUnMinuto()) {
        incrementarHora();

        if(esHoraInicial()) {
            incrementarDia();
        }
    }

    // Limpieza de transistores display
    for(int i = 0; i < 4; i++) {
        Chip_GPIO_SetValue(LPC_GPIO_PORT, DISPLAY_GPIO_PORT, digitoDisplay[i]);
    }

    // Barrido display hora
    if(counter % periodoHora < periodoHora / 2 && (counter % 4 == 0 || counter % 4 == 1)) {
        Chip_GPIO_ClearValue(LPC_GPIO_PORT, DISPLAY_GPIO_PORT, digitoDisplay[counter % 4]);
        intToBcd(displayHora[counter % 4]);
    }

    // Muestra LED ':'
    if(counter % PERIODO_TITILAR < PERIODO_TITILAR / 2) {
        Chip_GPIO_SetValue(LPC_GPIO_PORT, LEDP_GPIO_PORT, 1 << LEDP_GPIO_PIN);
    } else {
        Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDP_GPIO_PORT, 1 << LEDP_GPIO_PIN);
    }

    // Barrido display minutos
    if(counter % periodoMinuto < periodoMinuto / 2 && (counter % 4 == 2 || counter % 4 == 3)) {
        Chip_GPIO_ClearValue(LPC_GPIO_PORT, DISPLAY_GPIO_PORT, digitoDisplay[counter % 4]);
        intToBcd(displayHora[counter % 4]);
    }

    // Barrido led dia
    if(counter % periodoDias < periodoDias / 2) {
        if(flagMosrarDiaSolo) {
            clearDias();
            Chip_GPIO_SetValue(LPC_GPIO_PORT, diaToLedGpioPort[displayDia], 1 << diaToLedGpioPin[displayDia]);
        } else {
            clearDias();
            for(int i = 0; i < sizeof(displayVectorDia); i++) {
                if(displayVectorDia[i]) {
                    Chip_GPIO_SetValue(LPC_GPIO_PORT, diaToLedGpioPort[i], 1 << diaToLedGpioPin[i]);
                }
            }
        }
    } else {
        if(flagMosrarDiaSolo) {
            Chip_GPIO_ClearValue(LPC_GPIO_PORT, diaToLedGpioPort[displayDia], 1 << diaToLedGpioPin[displayDia]);
        } else {
            for(int i = 0; i < sizeof(displayVectorDia); i++) {
                if(displayVectorDia[i]) {
                    Chip_GPIO_ClearValue(LPC_GPIO_PORT, diaToLedGpioPort[i], 1 << diaToLedGpioPin[i]);
                }
            }
        }

    }

    // Incremento contador
    counter++;
}

void initHw(){

  Chip_GPIO_Init(LPC_GPIO_PORT);


  // Configurar TRANSISTOR 1 - DECENA HORA
  Chip_SCU_PinMux(DECHORA_MUX_GROUP, DECHORA_MUX_PIN, MD_PUP, FUNC0);
  //Chip_GPIO_ClearValue(LPC_GPIO_PORT, DECHORA_GPIO_PORT, 1 << DECHORA_GPIO_PIN);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, DECHORA_GPIO_PORT, 1 << DECHORA_GPIO_PIN, SALIDA);


  // Configurar TRANSISTOR 2 - UNIDAD HORA
  Chip_SCU_PinMux(DECHORA_MUX_GROUP, UNIHORA_MUX_PIN, MD_PUP, FUNC0);
  //Chip_GPIO_ClearValue(LPC_GPIO_PORT, UNIHORA_GPIO_PORT, 1 << UNIHORA_GPIO_PIN);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, UNIHORA_GPIO_PORT, 1 << UNIHORA_GPIO_PIN, SALIDA);

  // Configurar TRANSISTOR 3 - DECENA MINUTO
  Chip_SCU_PinMux(DECMIN_MUX_GROUP, DECMIN_MUX_PIN, MD_PUP, FUNC0);
  //Chip_GPIO_ClearValue(LPC_GPIO_PORT, DECHORA_GPIO_PORT, 1 << DECHORA_GPIO_PIN);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, DECMIN_GPIO_PORT, 1 << DECMIN_GPIO_PIN, SALIDA);


  // Configurar TRANSISTOR 4 - UNIDAD MINUTO
  Chip_SCU_PinMux(UNIMIN_MUX_GROUP, UNIMIN_MUX_PIN, MD_PUP, FUNC0);
  //Chip_GPIO_ClearValue(LPC_GPIO_PORT, UNIHORA_GPIO_PORT, 1 << UNIHORA_GPIO_PIN);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, UNIMIN_GPIO_PORT, 1 << UNIMIN_GPIO_PIN, SALIDA);

// CONFIGURA LED 'A'
  Chip_SCU_PinMux(LEDA_MUX_GROUP, LEDA_MUX_PIN, MD_PUP, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDA_GPIO_PORT, 1 << LEDA_GPIO_PIN, SALIDA);
  //Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDA_GPIO_PORT, 1 << LEDA_GPIO_PIN);

// CONFIGURA LED 'B'
  Chip_SCU_PinMux(LEDB_MUX_GROUP, LEDB_MUX_PIN, MD_PUP, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDB_GPIO_PORT, 1 << LEDB_GPIO_PIN, SALIDA);
  //Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDB_GPIO_PORT, 1 << LEDB_GPIO_PIN);

// CONFIGURA LED 'C'
  Chip_SCU_PinMux(LEDC_MUX_GROUP, LEDC_MUX_PIN, MD_PUP, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDC_GPIO_PORT, 1 << LEDC_GPIO_PIN, SALIDA);
  //Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDC_GPIO_PORT, 1 << LEDC_GPIO_PIN);

// CONFIGURA LED 'D'
  Chip_SCU_PinMux(LEDD_MUX_GROUP, LEDD_MUX_PIN, MD_PUP, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDD_GPIO_PORT, 1 << LEDD_GPIO_PIN, SALIDA);
  //Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDD_GPIO_PORT, 1 << LEDD_GPIO_PIN);

// CONFIGURA LED 'E'
  Chip_SCU_PinMux(LEDE_MUX_GROUP, LEDE_MUX_PIN, MD_PUP, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDE_GPIO_PORT, 1 << LEDE_GPIO_PIN, SALIDA);
  //Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDE_GPIO_PORT, 1 << LEDE_GPIO_PIN);

// CONFIGURA LED 'F'
  Chip_SCU_PinMux(LEDF_MUX_GROUP, LEDF_MUX_PIN, MD_PUP, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDF_GPIO_PORT, 1 << LEDF_GPIO_PIN, SALIDA);
  //Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDF_GPIO_PORT, 1 << LEDF_GPIO_PIN);

// CONFIGURA LED 'G'
  Chip_SCU_PinMux(LEDG_MUX_GROUP, LEDG_MUX_PIN, MD_PUP, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDG_GPIO_PORT, 1 << LEDG_GPIO_PIN, SALIDA);
  //Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDG_GPIO_PORT, 1 << LEDG_GPIO_PIN);

  // CONFIGURA LED ':'
    Chip_SCU_PinMux(LEDP_MUX_GROUP, LEDP_MUX_PIN, MD_PUP, FUNC0);
    Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDP_GPIO_PORT, 1 << LEDP_GPIO_PIN, SALIDA);
    //Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDG_GPIO_PORT, 1 << LEDG_GPIO_PIN);

  // Configurar funcion y caracteristicas electricas del terminal
 // Chip_SCU_PinMux(LED1_MUX_GROUP, LED1_MUX_PIN, MD_PUP, FUNC4);
// Poner los bit digitales en cero

// Configurar los bits digitales como salidas
//  Chip_GPIO_SetDir(LPC_GPIO_PORT, LED1_GPIO_PORT, 1 << LED1_GPIO_PIN, SALIDA);
//  Chip_GPIO_ClearValue(LPC_GPIO_PORT, LED1_GPIO_PORT, 1 << LED1_GPIO_PIN);

//  CONFIGURA LED DOMINGO
 Chip_SCU_PinMux(LEDDOM_MUX_GROUP, LEDDOM_MUX_PIN, MD_PUP, FUNC4);
 Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDDOM_GPIO_PORT, 1 << LEDDOM_GPIO_PIN, SALIDA);
 Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDDOM_GPIO_PORT, 1 << LEDDOM_GPIO_PIN);

//  CONFIGURA LED LUNES
  Chip_SCU_PinMux(LEDLUN_MUX_GROUP, LEDLUN_MUX_PIN, MD_PUP, FUNC4);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDLUN_GPIO_PORT, 1 << LEDLUN_GPIO_PIN, SALIDA);
  Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDLUN_GPIO_PORT, 1 << LEDLUN_GPIO_PIN);

//  CONFIGURA LED MARTES
  Chip_SCU_PinMux(LEDMAR_MUX_GROUP, LEDMAR_MUX_PIN, MD_PUP, FUNC4);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDMAR_GPIO_PORT, 1 << LEDMAR_GPIO_PIN, SALIDA);
  Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDMAR_GPIO_PORT, 1 << LEDMAR_GPIO_PIN);

//  CONFIGURA LED MIERCOLES
  Chip_SCU_PinMux(LEDMIE_MUX_GROUP, LEDMIE_MUX_PIN, MD_PUP, FUNC4);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDMIE_GPIO_PORT, 1 << LEDMIE_GPIO_PIN, SALIDA);
  Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDMIE_GPIO_PORT, 1 << LEDMIE_GPIO_PIN);

//  CONFIGURA LED JUEVES
  Chip_SCU_PinMux(LEDJUE_MUX_GROUP, LEDJUE_MUX_PIN, MD_PUP, FUNC4);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDJUE_GPIO_PORT, 1 << LEDJUE_GPIO_PIN, SALIDA);
  Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDJUE_GPIO_PORT, 1 << LEDJUE_GPIO_PIN);

//  CONFIGURA LED VIERNES
  Chip_SCU_PinMux(LEDVIE_MUX_GROUP, LEDVIE_MUX_PIN, MD_PUP, FUNC4);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDVIE_GPIO_PORT, 1 << LEDVIE_GPIO_PIN, SALIDA);
  Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDVIE_GPIO_PORT, 1 << LEDVIE_GPIO_PIN);

//  CONFIGURA LED SABADO
  Chip_SCU_PinMux(LEDSAB_MUX_GROUP, LEDSAB_MUX_PIN, MD_PUP, FUNC4);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, LEDSAB_GPIO_PORT, 1 << LEDSAB_GPIO_PIN, SALIDA);
  Chip_GPIO_ClearValue(LPC_GPIO_PORT, LEDSAB_GPIO_PORT, 1 << LEDSAB_GPIO_PIN);

// CONFIGURA BUZZER
  Chip_SCU_PinMux(BUZZER_MUX_GROUP, BUZZER_MUX_PIN, MD_PUP, FUNC4);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, BUZZER_GPIO_PORT, 1 << BUZZER_GPIO_PIN, SALIDA);
  Chip_GPIO_ClearValue(LPC_GPIO_PORT, BUZZER_GPIO_PORT, 1 << BUZZER_GPIO_PIN);

//  CONFIGURA MODO ALARMA
  Chip_SCU_PinMux(MODOALRM_MUX_GROUP, MODOALRM_MUX_PIN, MD_PDN|MD_EZI|MD_ZI, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, MODOALRM_GPIO_PORT, 1 << MODOALRM_GPIO_PIN, ENTRADA);

//  CONFIGURA MODO SET HORA
  Chip_SCU_PinMux(SETHORA_MUX_GROUP, SETHORA_MUX_PIN, MD_PDN|MD_EZI|MD_ZI, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, SETHORA_GPIO_PORT, 1 << SETHORA_GPIO_PIN, ENTRADA);

// CONFIGURA TECLADO

  // CONFIGURA FILAS
  Chip_SCU_PinMux(FILA1_MUX_GROUP, FILA1_MUX_PIN, MD_PUP, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, FILA1_GPIO_PORT, 1 << FILA1_GPIO_PIN, SALIDA);
  Chip_GPIO_SetValue(LPC_GPIO_PORT, FILA1_GPIO_PORT, 1 << FILA1_GPIO_PIN);

  Chip_SCU_PinMux(FILA2_MUX_GROUP, FILA2_MUX_PIN, MD_PUP, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, FILA2_GPIO_PORT, 1 << FILA2_GPIO_PIN, SALIDA);
  Chip_GPIO_SetValue(LPC_GPIO_PORT, FILA2_GPIO_PORT, 1 << FILA2_GPIO_PIN);

  Chip_SCU_PinMux(FILA3_MUX_GROUP, FILA3_MUX_PIN, MD_PUP, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, FILA3_GPIO_PORT, 1 << FILA3_GPIO_PIN, SALIDA);
  Chip_GPIO_SetValue(LPC_GPIO_PORT, FILA3_GPIO_PORT, 1 << FILA3_GPIO_PIN);

  Chip_SCU_PinMux(FILA4_MUX_GROUP, FILA4_MUX_PIN, MD_PUP, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, FILA4_GPIO_PORT, 1 << FILA4_GPIO_PIN, SALIDA);
  Chip_GPIO_SetValue(LPC_GPIO_PORT, FILA4_GPIO_PORT, 1 << FILA4_GPIO_PIN);

  //CONFIGURA COLUMNAS
  Chip_SCU_PinMux(COL1_MUX_GROUP, COL1_MUX_PIN, MD_PDN|MD_EZI|MD_ZI, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, COL1_GPIO_PORT, 1 << COL1_GPIO_PIN, ENTRADA);
  Chip_GPIO_ClearValue(LPC_GPIO_PORT, COL1_GPIO_PORT, 1 << COL1_GPIO_PIN);

  Chip_SCU_PinMux(COL2_MUX_GROUP, COL2_MUX_PIN, MD_PDN|MD_EZI|MD_ZI, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, COL2_GPIO_PORT, 1 << COL2_GPIO_PIN, ENTRADA);
  Chip_GPIO_ClearValue(LPC_GPIO_PORT, COL2_GPIO_PORT, 1 << COL2_GPIO_PIN);

  Chip_SCU_PinMux(COL3_MUX_GROUP, COL3_MUX_PIN, MD_PDN|MD_EZI|MD_ZI, FUNC0);
  Chip_GPIO_SetDir(LPC_GPIO_PORT, COL3_GPIO_PORT, 1 << COL3_GPIO_PIN, ENTRADA);
  Chip_GPIO_ClearValue(LPC_GPIO_PORT, COL3_GPIO_PORT, 1 << COL3_GPIO_PIN);



}

int leerPosicionSwitch() {
    // Construyo un nro entre 0 y 2 con las lecturas de los pines alarma y setHora
    return (Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,MODOALRM_GPIO_PORT,MODOALRM_GPIO_PIN) << 1)
           + Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,SETHORA_GPIO_PORT,SETHORA_GPIO_PIN);
}


// Se cambio de valor bool a uint8_t
uint8_t leerTeclado(){

    for(int j = 0; j < sizeof(portFilas); j++) {
        Chip_GPIO_ClearValue(LPC_GPIO_PORT, portFilas[j], 1 << pinFilas[j]);
    }

    for(int j = 0; j < sizeof(portFilas); j++) {
        Chip_GPIO_SetValue(LPC_GPIO_PORT, portFilas[j], 1 << pinFilas[j]);
        delay(100);
        for(int i = 0; i < sizeof(portColumnas); i++) {
            if(Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, portColumnas[i], pinColumnas[i])) {
                return mapaTeclado[j][i];
            }
        }
        Chip_GPIO_ClearValue(LPC_GPIO_PORT, portFilas[j], 1 << pinFilas[j]);
    }

    return 12;
}

void prenderDia(uint8_t dia){
    if(dia >= DOMINGO || dia <= SABADO) {
        displayDia = dia;
    }
}

void switchDia(uint8_t dia) {
    if(dia >= DOMINGO || dia <= SABADO) {
        displayVectorDia[dia] = !displayVectorDia[dia];
    }


}

void display(uint8_t display[]){
    // Solo debo pasar el valor a la variable interna, la funcion periodica de interrupcion
    // se encarga de pasar displayHora a la placa
    memcpy(displayHora, display, sizeof(displayHora));
}

void intToBcd(uint8_t digito){

    Chip_GPIO_SetValue(LPC_GPIO_PORT,LED_GPIO_PORT ,todos);
    if (digito <= 9) {
        Chip_GPIO_ClearValue(LPC_GPIO_PORT, LED_GPIO_PORT, segmentos[digito]);
    } else {
        Chip_GPIO_ClearValue(LPC_GPIO_PORT, LED_GPIO_PORT, segmentos[10]);
    }

}

void clearDias() {
    for(int i = 0; i < 7; i++) {
        Chip_GPIO_ClearValue(LPC_GPIO_PORT, diaToLedGpioPort[i], 1 << diaToLedGpioPin[i]);
    }
}

void delay(int ms) {
    int c = counter;

    while (counter <= c + ms);
}


// Sonar alarma

void BuzzerOn(void) {

    /*Se debe preguntar si la Hora es la misma que la de SET_ALARMA.
     * Entonces se prende el alarma con la rutina
     */

    //Chip_GPIO_SetValue(LPC_GPIO_PORT, BUZZER_GPIO_PORT, 1 << BUZZER_GPIO_PIN);

    /* Luego se deberia preguntar si seguimos en el modo de alarma o ya pasamos al modo switch_normal.
     * Si ya estamos en el modo switch_normal se deberia apagar la alarma.
     *
     */
    //Chip_GPIO_SetValue(LPC_GPIO_PORT, BUZZER_GPIO_PORT, 1 << BUZZER_GPIO_PIN);

}


void titilarHora() {
    stopTitilar();
    flagTitilarHora = 1;
}

void titilarMinuto() {
    stopTitilar();
    flagTitilarMinuto = 1;
}

void titilarDias() {
    flagTitilarDias = 1;
}

void stopTitilar() {
    flagTitilarHora = 0;
    flagTitilarMinuto = 0;
    flagTitilarDias = 0;
}

void stopTitilarDias() {
    flagTitilarDias = 0;
}

void mostrarDiasAlarma() {
    clearDias();
    flagMosrarDiaSolo = 0;
}

void mostrarDiaSolo() {
    clearDias();
    flagMosrarDiaSolo = 1;
}

void switchRutinaNormal(int nuevoValor) {
    esRutinaNormal = nuevoValor;
}

int pasoUnMinuto() {
    if(counter > counterMinuto + MSEG_POR_MINUTO) { // paso un minuto
        counterMinuto = counter;
        return 1;
    } else {
        return 0;
    }
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


