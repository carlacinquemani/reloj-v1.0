#include "alarma.h"
#include <string.h>
#include <stdio.h>

static uint8_t horaAlarmaActual[4];
static int diasAlarmaActual[7];

void inicializarAlarma() {
    memset(horaAlarmaActual, 0, sizeof(horaAlarmaActual));
    memset(diasAlarmaActual, 0, sizeof(diasAlarmaActual));
}

void setHoraAlarma(int hora) {
    if(hora <= MAX_HORA && hora >= 0) {
        horaAlarmaActual[0] = hora / 10;
        horaAlarmaActual[1] = hora % 10;
    }
}

void setMinutoAlarma(int minuto) {
    if(minuto <= MAX_MINUTO && minuto >= 0) {
        horaAlarmaActual[2] = minuto / 10;
        horaAlarmaActual[3] = minuto % 10;
    }
}

void switchDiaAlarma(int dia) {
    diasAlarmaActual[dia] = !diasAlarmaActual[dia];
}

uint8_t * obtenerHoraAlarma() {
    return horaAlarmaActual;
}

int * obtenerDiasAlarma() {
    return diasAlarmaActual;
}
