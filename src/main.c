/* Copyright 2014, 2015 Mariano Cerdeiro
 * Copyright 2014, Pablo Ridolfi
 * Copyright 2014, Juan Cecconi
 * Copyright 2014, Gustavo Muro
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


/*==================[inclusions]=============================================*/
#include "main.h"
#include "driver.h"
#include "hora.h"
#include "dia.h"
#include "alarma.h"

#ifndef CPU
#error CPU shall be defined
#endif
#if (lpc4337 == CPU)
#include "chip.h"
#elif (mk60fx512vlq15 == CPU)
#else
#endif

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/
/**
 * @brief Inicializa el Software
 *
 * Realiza la inicializacion de variables y estructuras de datos usadas
 * internamente por el programa y funciones
 */
void initSw(void);

/**
 * @brief Ejecuta la rutina normal
 *
 * Realiza las tareas en modo normal, es decir el funcionamiento del reloj
 * En este modo no se aceptan entradas del teclado.
 *
 * @param hora  El vector de horaActual
 * @param dia   Puntero al dia actual
 */
void rutinaNormal();

/**
 * @brief Ejecuta la rutina de configuracion de hora
 *
 * Realiza la tarea de configuracion de hora leyendo las entradas del teclado
 */
void rutinaSetHora(void);

/**
 * @brief Ejecuta la rutina de configuracion de alarma
 *
 * Realiza la tarea de configuracion de alarma leyendo las entradas del teclado
 */
void rutinaSetAlarma(void);


void initHw();
void configurarInterrupcion();


/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/
extern uint8_t tecla;

/*==================[internal functions definition]==========================*/
void initSw(void) {
    inicializarHora();
    inicializarDia();
}

void rutinaNormal() {
    stopTitilar();
    stopTitilarDias();
    display(obtenerHora());
    prenderDia(obtenerDiaActual());
}

void rutinaSetHora() {
    // TODO implementar

    uint8_t horaTest[4] = {0};
    tecla = leerTeclado();
    horaTest[3] = tecla;
    display(horaTest);
    titilarHora();
    titilarDias();

    /*
    modificarDecena(1);
    copiarHoraActualParaSet();
    display(obtenerHoraSet());
    titilarHora();
    do {
        tecla = leerTeclado();
        agregarDigitoHora(tecla);
        display(obtenerHoraSet());
    } while(tecla != 10 || tecla != 11);

    titilarMinuto();
    do {
        tecla = leerTeclado();
        agregarDigitoMinuto(tecla);
        display(obtenerHoraSet());
    } while(tecla != 10 || tecla != 11);
    */
}

void rutinaSetAlarma() {

    // TODO implementar
    uint8_t horaTest[4] = {0};
    display(horaTest);
    titilarMinuto();
}

/*==================[external functions definition]==========================*/
/** \brief Main function
 *
 * This is the main entry point of the software.
 *
 * \returns 0
 *
 * \remarks This function never returns. Return value is only to avoid compiler
 *          warnings or errors.
 */
int main(void) {

    /* Declaracion de variables locales */
    int posicionSwitch;


    /* Inicializacion HW y SW */
    initHw();
    configurarInterrupcion();
    initSw();

    setearHora(23, 57);

     while(1) {

       /* programa principal */
       posicionSwitch = leerPosicionSwitch();

       switch (posicionSwitch) {
       case SWITCH_NORMAL:
           rutinaNormal();
           break;
       case SWITCH_SET_HORA:
           rutinaSetHora();
           break;
       case SWITCH_SET_ALARMA:
           rutinaSetAlarma();
           break;
       default:
           break;
       }


       /* RUTINA DE PRUEBA
        * Si se necesitan hacer pruebas para alguna funcion (y se necesita subir el cambio)
        * escribir la llamada a continuacion anulando la llamada a las rutinas que ya tengan implementacion
        */

    }
    return 0;
}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

